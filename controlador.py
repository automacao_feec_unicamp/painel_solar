import ephem
import numpy as np
import datetime
import time as t
import smbus
import math
import RPi.GPIO as GPIO
import cv2
import spidev
from picamera import PiCamera
from picamera.array import PiRGBArray
from pysolar.solar import *

AJUSTE=0
# Define os pinos do GPIO que serao utilizados para ativacao dos motores
# Define a latitude e a longitude da regiao que ira atuar
PIN_1 = 18
PIN_2 = 22
Latitude = -22.8226801
Longitude = -47.0850446
RangeAnguloMecanico = 80
AmplitudeMovimento = 6
IntervaloMovimento = 5

# Calcula a duracao do dia solar em minutos para o proximo dia
def calculaDiaSolarMinutos(Latitude, Longitude):

	# Define o ponto de observacao com sua posicao geografica
	observer = ephem.Observer()
	observer.lat = str(Latitude)
	observer.long = str(Longitude)

	# Define as posicoes do sol
	sun = ephem.Sun()
	sun.compute()

	# Funcao forcada para pegar o proximo dia , apenas para testes
	horaInicioSolar = ephem.localtime(observer.next_rising(sun, start='2016/10/24'))
	horaFinalSolar = ephem.localtime(observer.next_setting(sun))
	# Executa o calculo do final do dia menos o inicio
	diaSolar = horaFinalSolar - horaInicioSolar
	diaSolarMinutos = np.round(diaSolar.total_seconds()/60)

	arqFinal = open('archives/diaSolar.txt', 'w')
	textoFinal = diaSolar.strftime("%H:%M")
	arqFinal.write(textoFinal)
	arqFinal.close()

	# Retorna os dados referentes a duracao em minutos do dia solar
	return diaSolarMinutos

# Calcula o meio dia solar para a data especificada
def calculaMeioDiaSolar(Ano, Mes, Dia, Latitude, Longitude):

	#Vetores locais para calculo
	vetorAltitude = []
	vetorMomento = []

	# Para cada hora e minuto do dia adiciona ao vetor os momentos e a altitude solar correspondente
	for hora in range(0,24):
		for minuto in range(0,60):
			data = datetime.datetime(Ano,Mes,Dia,hora,minuto,0,0)
			altitude = get_altitude(Latitude,Longitude,data)
			vetorAltitude.append(altitude)
			vetorMomento.append(data)
	# Define o index do vetor momento como a posicao de maior altitude solar do dia
	meioDiaSolarIndex = np.argmax(vetorAltitude)
	meioDiaSolar = vetorMomento[meioDiaSolarIndex]

	arqFinal = open('archives/meioDiaSolar.txt', 'w')
	textoFinal = meioDiaSolar.strftime("%H:%M")
	arqFinal.write(textoFinal)
	arqFinal.close()

	return meioDiaSolar

# Calcula variacao entre do angulo mecanico conforme as 6 horas de sol
def calculaVariacaoAnguloMecanico(Ano,Mes,Dia,Latitude,Longitude,RangeAnguloMecanico,AmplitudeMovimento, MeioDiaSolar):

	# Define a direfeca da hora solar
	diferencaHoraRealSolar = MeioDiaSolar - datetime.datetime(Ano,Mes,Dia,12,0,0) 

	# Calcula o inicio e o final de atuacao dos motores conforme horario solar
	inicioMovimento = datetime.datetime(Ano,Mes,Dia,12,0,0) + diferencaHoraRealSolar - datetime.timedelta(0,0,0,0,0,(AmplitudeMovimento/2))
	finalMovimento = datetime.datetime(Ano,Mes,Dia,12,0,0) + diferencaHoraRealSolar + datetime.timedelta(0,0,0,0,0,(AmplitudeMovimento/2))

	# Calcula angulo inicial e final conforme calculo do azimuth
	anguloInicial = get_azimuth(Latitude,Longitude,inicioMovimento)
	anguloFinal = get_azimuth(Latitude,Longitude,finalMovimento)

	# Define o range de atuacao 
	rangeAnguloAzimute = np.absolute(anguloInicial) - np.absolute(anguloFinal)
	relacaoAnguloAzimuteMecanico = rangeAnguloAzimute / RangeAnguloMecanico

	# Salva a hora de inicio e final do movimento em arquivo para recuperacao no site
	arqInicio = open('archives/inicioMovimento.txt', 'w')
	textoInicio = inicioMovimento.strftime("%H:%M")
	arqInicio.write(textoInicio)
	arqInicio.close()

	arqFinal = open('archives/finalMovimento.txt', 'w')
	textoFinal = finalMovimento.strftime("%H:%M")
	arqFinal.write(textoFinal)
	arqFinal.close()

	return relacaoAnguloAzimuteMecanico,inicioMovimento,finalMovimento

# Define o movimento que será feito pela placa de acordo com os dados estabelecidos no inicio do codigo
def defineMovimento(AmplitudeMovimento,IntervaloMovimento,InicioMovimento,RangeAnguloMecanico,horaMomento,grau):

	#Define a quantidade de pulos que acontecerá
	totalMinutosFuncionamento = AmplitudeMovimento * 60
	numeroPulos = totalMinutosFuncionamento/IntervaloMovimento
	tamanhoPulo = RangeAnguloMecanico/numeroPulos
	#horaMomento = datetime.datetime.now()
	
	# Cria vetor de pulos e acerta o range do pulo para movimentacao e adequacao do movimento
	vetorPulo = []
	vetorHoraMomento = []
	vetorDiferenca = []

	# Para cada pulo dentro do range, define a diferenca que deve ser movimentada
	for pulo in range(0,int(numeroPulos)+1):
		momentoPulo = InicioMovimento + datetime.timedelta(0,0,0,0,IntervaloMovimento*pulo,0)
		vetorPulo.append(momentoPulo)
		vetorHoraMomento.append(horaMomento)
		vetorDiferenca.append(np.absolute(momentoPulo - horaMomento))

	# Define a menor diferenca e o pulo atual
	indiceMenorDiferenca = np.argmin(vetorDiferenca)
	horaMenorDiferenca = vetorPulo[indiceMenorDiferenca]

	# Define as condicoes para definiciao do pulo
	if(horaMomento<horaMenorDiferenca):
		indiceMenorDiferenca = indiceMenorDiferenca-1
		horaMenorDiferenca = vetorPulo[indiceMenorDiferenca]

	# Define o angulo total como um numero inteiro 
	puloAngularTotal = np.round(indiceMenorDiferenca*tamanhoPulo)
	
	print ("Graus mecânicos que se encontra a placa: ",grau)
	print("")

	grauMovimentoTotal = (-40) + puloAngularTotal
	print ("Graus meânicos que deve se orientar: ",grauMovimentoTotal)
	return grauMovimentoTotal,indiceMenorDiferenca

# Funcao para ligar motor, de acordo com sentido, por tempo determinado
def ligarMotor(PIN_1,PIN_2,Sentido,Tempo):
	if(Sentido == "+"):
		iniciaRotacaoMotor(PIN_1,PIN_2)
		print("Sentido horário")
		t.sleep(Tempo)
		pausaRotacaoMotor(PIN_1,PIN_2)
	elif(Sentido=="-"):
		iniciaRotacaoMotor(PIN_2,PIN_1)
		print("Sentido anti-horário")
		t.sleep(Tempo)
		pausaRotacaoMotor(PIN_1,PIN_2)
	else:
		pausaRotacaoMotor(PIN_1,PIN_2)
	t.sleep(0.2)

# Ativa o motor de acordo com a diferenca entre angulo atual e o requerido pelas condicoes da execucao
def posicionarAngulo(PIN_1,PIN_2,AnguloRequerido):
	anguloTotal = recebeGrau()
	# Define uma margem de erro de um grau para cima ou para baixo
	while(np.absolute(anguloTotal - AnguloRequerido)>=1):
		anguloTotal = recebeGrau()
		t.sleep(0.1)
		print("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")
		print("@@@           Angulo atual: ",anguloTotal)
		print("@@@           Angulo requerido",AnguloRequerido)
		print("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")

		if(anguloTotal>AnguloRequerido):
			ligarMotor(PIN_1,PIN_2,'-',0.1)
		else:
			ligarMotor(PIN_1,PIN_2,'+',0.1)	

# Inicia a rotacao do motor atraves tendo como parametros de entrada os pinos que serao ativados
def iniciaRotacaoMotor(x,y):
    GPIO.output(x, GPIO.HIGH)
    GPIO.output(y, GPIO.LOW)

# Pausa a rotacao do motor tendo como parametros os pinos que deverao ser desativados
def pausaRotacaoMotor(x,y):
    GPIO.output(x, GPIO.LOW)
    GPIO.output(y, GPIO.LOW)

# Le os dados do acelerometro
def read_word(adr):
	high = bus.read_byte_data(address, adr)
	low = bus.read_byte_data(address, adr+1)
	val = (high << 8) + low
	if (val >= 0x8000):
		return -((65535 - val) + 1)
	else:
		return val
# Realiza funcoes matematicas
def dist(a,b):
    return math.sqrt((a*a)+(b*b))

# Retorna a rotacao do eixo y
def get_y_rotation(x,y,z):
    radians = math.atan2(x, dist(y,z))
    return -math.degrees(radians)

# Retorna a rotacao do eixo x
def get_x_rotation(x,y,z):
    radians = math.atan2(y, dist(x,z))
    return math.degrees(radians)

# Recebe todas as informacoes do acelerometro e devolve o grau atual
def recebeGrau():
	accel_xout = read_word(0x3b)
	accel_xout_scaled = accel_xout / 16384.0
	accel_yout = read_word(0x3d)
	accel_yout_scaled = accel_yout / 16384.0
	accel_zout = read_word(0x3f)
	accel_zout_scaled = accel_zout / 16384.0
	grau = get_x_rotation(accel_xout_scaled, accel_yout_scaled, accel_zout_scaled)
	return (np.round(grau))

def filtroBlobGrande(imagem):
	original = imagem.copy()
	_,contours, hierarchy = cv2.findContours( imagem.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)
	areaMaior = 0
	area = 0
	indiceArea = 0
	indiceAreaMaior = -1

	for c in contours:
		indiceArea+=1
		area = cv2.contourArea(c)
		moments = cv2.moments(c)
		if(area>10000):
			if area>=areaMaior:
				areaMaior = area
				indiceAreaMaior = indiceArea
	
	if(indiceAreaMaior==-1):
		resultado=imagem
		flag = 0
	else:
		cnt = contours[indiceAreaMaior-1]
		pp = cv2.drawContours(imagem, cnt, -1, 0, 255)
		resultado = cv2.bitwise_and(original,original,mask=~pp)
		flag=1
	return resultado,flag

def processaImagem(Foto):
	gray = cv2.cvtColor(Foto, cv2.COLOR_BGR2GRAY)
	radio=3
	gaussGray = cv2.GaussianBlur(gray, (radio,radio), 0)
	ret,binario = cv2.threshold(gaussGray,210,255,cv2.THRESH_BINARY)
	binarioBlob,flag = filtroBlobGrande(binario.copy())

	if(flag==0):
		eixoX=-1
	else:
		_,contours0, hierarchy = cv2.findContours( binarioBlob.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)
		for c in contours0:
		    # compute the center of the contour
			M = cv2.moments(c)

			if(M['m00']!=0):
				eixoX = int(M["m10"] / M["m00"])
			else:
				eixoX=-1

	return eixoX,binarioBlob
	
def movimentoCamera(toleranciaPixel,PIN_1,PIN_2):

	# initialize the camera and grab a reference to the raw camera capture
	camera = PiCamera()
	camera.resolution=(2592,1944)

	rawCapture = PiRGBArray(camera)
	t.sleep(0.1)
	validadeMovimento = 1
	
	larguraFoto=2592
	metadeLarguraFoto=larguraFoto/2
	while (validadeMovimento == 1):
		camera.capture(rawCapture, format="bgr")
		rawCapture.truncate(0)
		foto = rawCapture.array
		posicaoSolCamera,binarioBlob=processaImagem(foto)
		grau = recebeGrau()
		print("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")
		if((np.absolute(posicaoSolCamera-metadeLarguraFoto)>toleranciaPixel)&(grau>-40)&(grau<40)):
			#Movimento
			if(posicaoSolCamera>metadeLarguraFoto):  #Conferir Sentidos
				#Movimento sentido antihorario
				print("##-> Movimento camera: Antihorario")
				ligarMotor(PIN_1,PIN_2,'-',0.1)
			else:
				#Movimento sentido horario
				print("##-> Movimento camera: Horario")
				ligarMotor(PIN_1,PIN_2,'+',0.1)
		else:
			print("##-> Movimento camera: Sem movimento")
			validadeMovimento = 0
			camera.close() 

def ReadChannel(channel):
	adc = spi.xfer2([1,(8+channel)<<4,0])
	data = ((adc[1]&3) << 8) + adc[2]
	return data
 
# Function to convert data to voltage level,
# rounded to specified number of decimal places.
def ConvertVolts(data,places):
	volts = (data * 5.0) / float(1023)
	volts = round(volts,places)

	#arqFinal = open('archives/volts.txt', 'w')
	#textoFinal = volts
	#arqFinal.write(textoFinal)
	#arqFinal.close()

	return volts

# Define os pinos que irao atuar no motor e os define como saida de sinal
GPIO.setmode(GPIO.BOARD)
GPIO.setwarnings(False)
GPIO.setup(PIN_1, GPIO.OUT)
GPIO.setup(PIN_2, GPIO.OUT)

spi = spidev.SpiDev()
spi.open(0,0)

# Define a voltade de saida inicial como 0
pausaRotacaoMotor(PIN_1,PIN_2);

# Registradores gerenciadores de energia
power_mgmt_1 = 0x6b
power_mgmt_2 = 0x6c

# Define qual barramento I2C utilizado para o recolhimento de dados do acelerometro
bus = smbus.SMBus(1)

# Checagem da atividade do barramento
address = 0x68

# Define os valores dos registradores
bus.write_byte_data(address, power_mgmt_1, 0)

grau = recebeGrau();
horarioAtivo = []

now = datetime.datetime.now()
ultimoPulo=-1
horarioAtivo = calculaVariacaoAnguloMecanico(now.year,now.month,now.day,Latitude,Longitude,RangeAnguloMecanico,AmplitudeMovimento, calculaMeioDiaSolar(now.year, now.month, now.day, Latitude, Longitude))

#Spi Bus
spi = spidev.SpiDev()
spi.open(0,0)

while(1):
	# Recalcula o valor a cada iteracao
	grau = recebeGrau();
	now = datetime.datetime.now()-datetime.timedelta(0,0,0,0,AJUSTE,0)
	
	# Tempo dado para calcular os valores
	t.sleep(0.1)

	# Verificacao de posicao sobre o eixo X
	if((grau < 45) & (grau>(-45))):

		#print("Posicao da base mecanica em graus: ",grau)
		# Verifica a hora final do calculo para que encerre o processo
		
		if((now.hour >= 0) & (now.hour <= 1)):
		#if(now.hour == 0):
			horarioAtivo = calculaVariacaoAnguloMecanico(now.year,now.month,now.day,Latitude,Longitude,RangeAnguloMecanico,AmplitudeMovimento, calculaMeioDiaSolar(now.year, now.month, now.day, Latitude, Longitude))

		#Garantir que esta dentro do Range		
		#if((now.hour == horarioAtivo[1].hour) & (now.hour == horarioAtivo[1].minute))
		print("------------------------------------------------------------")
		print("Horario de inicio da ativacao: ",horarioAtivo[1])
		print("")
		print("Horario de final da ativacao: ",horarioAtivo[2])
		print("")
		print("Hora atual: ",now)
		
		if(now.second == 0):
			adc = ReadChannel(0)
			pot_volts = ConvertVolts(adc,4)
			corrente = (13800*pot_volts)-34535
			#correnteMedia = (corrente+correnteMedia)
			t.sleep(1)
			arquivo = open('/home/pi/Desktop/Develop/testeMovimento.csv','r')
			texto = arquivo.readlines()
			texto.append(str(now.hour))
			texto.append(":")
			texto.append(str(now.minute))
			texto.append(",")
			texto.append(str(corrente))
			print("Valor recolhido: ",corrente)
			texto.append('\n')
			arquivo = open('/home/pi/Desktop/Develop/testeMovimento.csv','w')
			arquivo.writelines(texto)
			arquivo.close()
		
		if((horarioAtivo[1]<=now) & (horarioAtivo[2]>=now)):	
			definicaoMovimento,indiceMenorDiferenca = defineMovimento(AmplitudeMovimento,IntervaloMovimento,horarioAtivo[1],RangeAnguloMecanico,now,grau)
			if(indiceMenorDiferenca>ultimoPulo):
				posicionarAngulo(PIN_1,PIN_2,definicaoMovimento)
				ultimoPulo=indiceMenorDiferenca
				movimentoCamera(10,PIN_1,PIN_2)
		else:
			t.sleep(0.1)
			pausaRotacaoMotor(PIN_1,PIN_2)
	else:

		pausaRotacaoMotor(PIN_1,PIN_2)
		print ("x rotation: " , get_x_rotation(accel_xout_scaled, accel_yout_scaled, accel_zout_scaled))

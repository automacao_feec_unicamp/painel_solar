# Painel Solar

![Video](https://gitlab.com/automacao_feec_unicamp/painel_solar/wikis/blob/video.mp4)

Controle de movimentação de painel solar utilizando Raspberry Pi.

Informações: [wiki](https://gitlab.com/automacao_feec_unicamp/painel_solar/wikis/home)

## Autores

Abel Alejandro Dueñas Rodriguez, Luiz Antonio de Sousa Ferreira
